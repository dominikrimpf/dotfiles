actions:
  pre:
    nvim_plug-install: test -e ~/.local/share/nvim/site/autoload/plug.vim || (curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim)
    tmux_plugin_manager_install: test -d ~/.tmux/plugins/tpm || (git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm)
  post:
    nvim_plug: nvim +PlugClean! +PlugInstall +PlugInstall! +qall
    tmux_plugins_install: ~/.tmux/plugins/tpm/bin/install_plugins
trans_install:
  osacompile: /usr/bin/osacompile -o "{1}" "{0}"
dynvariables:
  platform: uname -s | tr '[:upper:]' '[:lower:]'
  custom_go: stat /usr/local/go &> /dev/null && echo 'True' || echo ''
  bat: command -v bat &> /dev/null && echo 'True' || echo ''
  beyondcompare: command -v bcomp &> /dev/null && echo 'True' || echo ''
  ecsmgmt: command -v ecsmgmt &> /dev/null && echo 'True' || echo ''
  eza: command -v eza &> /dev/null && echo 'True' || echo ''
  fzf: command -v fzf &> /dev/null && echo 'True' || echo ''
  gibo: command -v gibo &> /dev/null && echo 'True' || echo ''
  git_delta: command -v delta &> /dev/null && echo 'True' || echo ''
  mbc: command -v mbc &> /dev/null && echo 'True' || echo ''
  meld: command -v meld &> /dev/null && echo 'True' || echo ''
  poetry: command -v poetry &> /dev/null && echo 'True' || echo ''
  poetry_path_hash: md5 -q -s "$(realpath $(which poetry))" | sed 's/\(.\{16\}\).*/\1/'
  pve_cli: command -v pve-cli &> /dev/null && echo 'True' || echo ''
  rbw: command -v rbw &> /dev/null && echo 'True' || echo ''
  ripgrep: command -v rg &> /dev/null && echo 'True' || echo ''
  rustup: command -v rustup &> /dev/null && echo 'True' || echo ''
  sshfs: command -v sshfs &> /dev/null && echo 'True' || echo ''
  synadm: command -v synadm &> /dev/null && echo 'True' || echo ''
  orbstack: if open -Ra "Orbstack"; then echo True; else echo False; fi
  rbw_username: rbw get --field username {{@@ rbw_entry_id @@}}
  rbw_url: rbw get --field uris {{@@ rbw_entry_id @@}}
variables:
  home_dir: "{{@@ env['HOME'] @@}}"
  rbw_entry_id: b5ac9b93-7b63-4314-b7eb-6366556897f8
config:
  backup: false
  banner: true
  create: true
  dotpath: dotfiles
  ignoreempty: false
  keepdot: false
  link_dotfile_default: nolink
  link_on_import: nolink
  longkey: false
  showdiff: true
  workdir: ~/.config/dotdrop
dotfiles:
  30_d_zsh_pre:
    dst: ~/.zsh/pre
    src: zsh/pre
    chmod: 750
    cmpignore:
      - '*zwc'
  31_d_zsh_post:
    dst: ~/.zsh/post
    src: zsh/post
    chmod: 750
    cmpignore:
      - '*zwc'
  40_f_zshrc:
    dst: ~/.zshrc
    src: zsh/zshrc
    chmod: 640
  41_f_p10k:
    dst: ~/.p10k.zsh
    src: zsh/p10k.zsh
    chmod: 640
  42_f_zprofile:
    dst: ~/.zprofile
    src: zsh/zprofile
    chmod: 640
  44_f_zsh_completions:
    dst: ~/.zsh/completions.zsh
    src: zsh/completions.zsh
    chmod: 640
  50_f_nvim-config:
    dst: ~/.config/nvim/init.vim
    src: nvim/init.vim
    chmod: 640
    actions:
      - nvim_plug-install
      - nvim_plug
  50_f_ssh-config:
    dst: ~/.ssh/config
    src: ssh/config
    chmod: 640
  50_f_bat-config:
    src: bat
    dst: ~/.config/bat/config
    chmod: 640
  51_f_rbw_config:
    dst: '{{@@ config_home @@}}/rbw/config.json'
    src: rbw.json
    chmod: 644
  52_f_poetry-config:
    dst: '{{@@ config_home @@}}/pypoetry/config.toml'
    src: pypoetry.toml
    chmod: 640
  53_f_loadsshkeys:
    dst: ~/.local/bin/load-ssh-keys
    src: scripts/load-ssh-keys
    chmod: '700'
  53_f_socks-proxy:
    dst: ~/.local/bin/socks-proxy
    src: scripts/socks-proxy
    chmod: '700'
  53_f_socks-proxy-keepalive:
    dst: ~/.local/bin/socks-proxy-keepalive
    src: scripts/socks-proxy-keepalive
    chmod: '700'
  55_d_tmux_plugin_manager:
    src:
    dst:
    actions:
      - tmux_plugin_manager_install
  55_f_tmux_config:
    src: tmux.conf
    dst: ~/.tmux.conf
    chmod: 640
    actions:
      - tmux_plugins_install
  60_f_git-config:
    dst: ~/.gitconfig
    src: git/config
    chmod: 640
  60_f_gitignore_global-macos:
    dst: ~/.gitignore_global
    src: git/ignore-macos
    chmod: 640
  60_f_gitignore_global-linux:
    dst: ~/.gitignore_global
    src: git/ignore-linux
    chmod: 640
  60_f_gitignore_global-windows:
    dst: ~/.gitignore_global
    src: git/ignore-windows
    chmod: 640
  61_f_git-config-net:
    dst: ~/{{@@ dev_folder @@}}/scc-net/.gitconfig
    src: git/config-net
    chmod: 640
  62_f_tigrc:
    src: tigrc
    dst: ~/.tigrc
    chmod: 640
  70_d_applescripts:
    dst: ~/Library/Scripts
    src: applescripts
  71_f_applescript_sshfs:
    dst: ~/Library/Scripts/mount-sshfs.scpt
    src: applescripts/mount-sshfs.applescript
    trans_install: osacompile
  71_f_applescript_sshfs_fse:
    dst: ~/Library/Scripts/mount-fse.scpt
    src: applescripts/mount-fse.applescript
    trans_install: osacompile
  71_f_applescript_sshfs_fse_ref:
    dst: ~/Library/Scripts/mount-fse-ref.scpt
    src: applescripts/mount-fse-ref.applescript
    trans_install: osacompile
  71_f_applescript_sshfs_fse_transfer:
    dst: ~/Library/Scripts/mount-fse-transfer.scpt
    src: applescripts/mount-fse-transfer.applescript
    trans_install: osacompile
  71_f_applescript_sshfs_umount:
    dst: ~/Library/Scripts/umount-sshfs.scpt
    src: applescripts/umount-sshfs.applescript
    trans_install: osacompile
  80_d_sway:
    dst: ~/.config/sway
    src: sway
    chmod: 750
  80_d_waybar:
    dst: ~/.config/waybar
    src: waybar
    chmod: 750
  90_f_gpg_config:
    dst: ~/.gnupg/gpg.conf
    src: gnupg/gpg.conf
    chmod: 600
  90_f_gpg-agent_config:
    dst: ~/.gnupg/gpg-agent.conf
    src: gnupg/gpg-agent.conf
    chmod: 600
profiles:
  default:
    dotfiles:
      - 30_d_zsh_pre
      - 31_d_zsh_post
      - 40_f_zshrc
      - 41_f_p10k
      - 42_f_zprofile
      - 44_f_zsh_completions
      - 50_f_nvim-config
      - 50_f_ssh-config
      - 50_f_bat-config
      - 52_f_poetry-config
      - 55_d_tmux_plugin_manager
      - 55_f_tmux_config
      - 60_f_git-config
      - 62_f_tigrc
  laptop:
    dotfiles:
      - 51_f_rbw_config
      - 53_f_loadsshkeys
      - 53_f_socks-proxy
      - 53_f_socks-proxy-keepalive
  linux:
    dotfiles:
      - 60_f_gitignore_global-linux
    include:
      - default
    variables:
      config_home: ~/.config
  macos:
    dotfiles:
      - 60_f_gitignore_global-macos
      - 90_f_gpg_config
      - 90_f_gpg-agent_config
    include:
      - default
    variables:
      config_home: ~/Library/Application Support
    dynvariables:
      brew_prefix: brew --prefix
  fedora:
    include:
      - default
      - linux
    variables:
  oswin:
    dotfiles:
      - 61_f_git-config-net
    include:
      - laptop
      - macos
    variables:
      dev_folder: code
  t400:
    include:
      - laptop
      - fedora
