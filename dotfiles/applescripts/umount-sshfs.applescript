-- Function: get mounted sshfs-Volumes
on getMountedVolumes()
	set mountedVolumes to do shell script "mount | grep osxfuse | cut -d' ' -f3"
	if mountedVolumes is equal to "" then
		log "No mounted sshfs Volumes"
		error number -128
	end if
	log "Mounted Volumes: " & mountedVolumes
	return mountedVolumes
end getMountedVolumes

-- Function: unmount all Volumes in given list
on unmountVolumes(volumeList)
	repeat with volume in volumeList
		do shell script "diskutil umount force " & volume
		log "Unmounted: " & volume
	end repeat
end unmountVolumes

set mountedVolumes to getMountedVolumes()
-- Split volumes-list-string to real list
set mountedVolumesList to paragraphs of mountedVolumes
unmountVolumes(mountedVolumesList)
