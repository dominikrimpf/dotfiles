-- SSH parameters 
set args to {sshRemote:"fse"}

-- SSHFS parameters
set remotePaths to {"/home/ref", "/home/transfer", "/home/dominikr", "/home/backup"}
set localPaths to {"~/mount/fse-ref", "~/mount/fse-transfer", "~/mount/fse-dominikr", "~/mount/fse-backup"}
set volumeNames to {"fse-ref", "fse-transfer", "fse-dominikr", "fse-backup"}
set volumesList to {"Referate", "Transfer", "Dominikr", "Backup"}
set mountVolumes to choose from list volumesList default items {"Referate", "Transfer"} with multiple selections allowed

set scriptPath to POSIX file "/Users/dominik/Library/Scripts/mount-sshfs.scpt"
set sshfsHelpers to load script scriptPath

repeat with a from 1 to count of volumesList
	if mountVolumes contains item a of volumesList then
		set parameters to args & {remotePath:(item a of remotePaths), localPath:(item a of localPaths), volumeName:(item a of volumeNames)}
		tell sshfsHelpers to mountSSHFS(parameters)
	end if
end repeat
