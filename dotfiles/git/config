[core]
  excludesfile = ~/.gitignore_global
{%@@ if git_delta @@%}
  pager = delta
{%@@ endif @@%}
[user]
  name = Dominik Rimpf
  email = dev@drimpf.de
{%@@ if profile in ["oswin"] @@%}
[includeIf "gitdir:~/{{@@ dev_folder @@}}/scc-net/"]
  path = ~/{{@@ dev_folder @@}}/scc-net/.gitconfig
{%@@ endif @@%}

[color]
  ui = true
[init]
  defaultBranch = main
[push]
  default = simple
  autoSetupRemote = true
[pull]
  rebase = true
[commit]
  verbose = true
[rebase]
  autostash = true
[tag]
  forceSignAnnotated = false
[gpg]
  program = gpg
[alias]
  co = checkout
  sw = switch
  s = status
  f = fetch --all -tpP
  push-f = push --force-with-lease
  push-t = push --tags
  push-new = !git push --set-upstream origin $(git rev-parse --abbrev-ref HEAD)
[filter "lfs"]
  clean = git-lfs clean -- %f
  smudge = git-lfs smudge -- %f
  process = git-lfs filter-process
  required = true
[hub]
  protocol = ssh

[url "ssh://dominikr@server-01.fs-etec.kit.edu/home/git/"]
  insteadof = fse:

{%@@ if git_delta @@%}
[interactive]
  diffFilter = delta --color-only

[delta]
  features = side-by-side line-numbers decorations
  whitespace-error-style = 22 reverse

[delta "decorations"]
  commit-decoration-style = bold yellow box ul
  file-style = bold yellow ul
  file-decoration-style = none
  hunk-header-decoration-style = yellow box
{%@@ endif @@%}

{%@@ if beyondcompare @@%}
{%@@ set guitool = "bcomp" @@%}
{%@@ elif meld @@%}
{%@@ set guitool = "meld" @@%}
{%@@ endif @@%}
{%@@ if guitool @@%}
[diff]
  guitool = {{@@ guitool @@}}
[merge]
  guitool = {{@@ guitool @@}}
{%@@ endif @@%}
{%@@ if beyondcompare @@%}
[difftool "bcomp"]
  trustExitCode = true
[mergetool "bcomp"]
  trustExitCode = true
{%@@ endif @@%}
{%@@ if meld @@%}
[difftool "meld"]
  trustExitCode = true
  cmd = open -W -a Meld --args \"$LOCAL\" \"$REMOTE\"
[mergetool "meld"]
  trustExitCode = true
  cmd = open -W -a Meld --args --auto-merge \"$LOCAL\" \"$BASE\" \"$REMOTE\" --output=\"$MERGED\"
  cmd = meld "$LOCAL" "$BASE" "$REMOTE" --output "$MERGED"
{%@@ endif @@%}

[diff "ansible-vault"]
  textconv = ansible-vault view
  cachetextconv = true
