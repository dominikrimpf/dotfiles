{#@@
# vim: ft=sshconfig
@@#}
{%@@ if orbstack @@%}
Include ~/.orbstack/ssh/config

{%@@ endif @@%}
# Fachschaft
# fse socks admin
host socks-fse-admin
  HostName admin.fs-etit.kit.edu
  User dominik

host socks-fse-*
  IdentityFile ~/.ssh/fse.ed25519
  DynamicForward 9091
  Compression yes
  SessionType none

host fse
  HostName server-01.fs-etec.kit.edu
  User dominikr
  IdentityFile ~/.ssh/fse.ed25519

host admin*.fs-etit.kit.edu
  User dominik
  ForwardAgent yes
  IdentityFile ~/.ssh/fse.ed25519

host *fs-etit.kit.edu *fs-etec.kit.edu *fs-etit.org !admin*.fs-etit.kit.edu
  User root
  ProxyJump admin.fs-etit.kit.edu
  IdentityFile ~/.ssh/fse-root-a.ed25519-sk
  IdentityFile ~/.ssh/fse-root-b.ed25519-sk

# bufata servers
host bufata1-ports
  HostName server-1.bufata-et.de
  # 389 dirsrv
  LocalForward 9000 127.0.1.1:389
  # MariaDB
  LocalForward 9001 127.0.0.1:3306
  # Postgres
  LocalForward 9002 127.0.0.1:5432
  # Etherpad lite
  LocalForward 9003 127.0.0.1:9001
  # openldap slapd
  LocalForward 9004 127.0.0.1:389
# bufata defaults
host bufata* *.bufata-et.de
  IdentityFile ~/.ssh/bufata.ed25519
  User root

# SCC
# matrix
host admin-1.matrix.scc.kit.edu
  User ha2931
  ForwardAgent yes
host *matrix.scc.kit.edu !admin-1.matrix.scc.kit.edu
  User root
  ProxyJump admin-1.matrix.scc.kit.edu
host *matrix.scc.kit.edu
  IdentityFile ~/.ssh/scc-matrix.ed25519

# misc servers
host tools-1
  HostName tools-1.scc.kit.edu
  User ha2931
  IdentityFile ~/.ssh/scc-user.ed25519
host opentext-scc
  HostName www.scc.kit.edu
  User scc-web-0027
  IdentityFile ~/.ssh/scc-user.ed25519
host opentext-matrix
  HostName www.matrix.kit.edu
  User scc-web-0058
  IdentityFile ~/.ssh/scc-user.ed25519

# tmn sockcs connections
host socks-tmn-1
  HostName entry-1.tmn.scc.kit.edu

host socks-tmn-2
  HostName entry-2.tmn.scc.kit.edu

host socks-tmn-li-cs
  HostName net-li-cs.scc.kit.edu

host socks-tmn-li-cn
  HostName net-li-cn.scc.kit.edu

host socks-tmn-*
  User arimpf
  IdentityFile ~/.ssh/scc-user.ed25519
  DynamicForward 9090
  Compression yes
  SessionType none

# net servers
host net-admin.tmn.scc.kit.edu ansible-2.tmn.scc.kit.edu
  ForwardAgent yes
host aruba-mm.scc.kit.edu
  HostName aruba-mm.scc.kit.edu
  User arimpf

# bmcs
host *bmc*.scc.kit.edu
  User root
  ProxyJump entry-1.tmn.scc.kit.edu

# net defaults
host *tmn.scc.kit.edu !entry-*.tmn.scc.kit.edu
  ProxyJump entry-1.tmn.scc.kit.edu

host *net.scc.kit.edu *tmn.scc.kit.edu net-*scc.kit.edu
  User arimpf

# scc defaults - defaults to "net"-settings because thats the moste machines i access
# THIS HAS TO BE AFTER ALL "FS" and similar *.kit.edu configs
host *kit.edu
  IdentityFile ~/.ssh/scc-user.ed25519
  IdentityFile ~/.ssh/scc-net-admin.ed25519

# asta
host asta
  HostName login.asta-kit.de
  User dominikr
  IdentityFile ~/.ssh/fse.ed25519

# private servers
host *dotdorm.de
  User root
  IdentityFile ~/.ssh/private-infrastructure-a.ed25519-sk
  IdentityFile ~/.ssh/private-infrastructure-b.ed25519-sk

# blechhaufen
host *blechhaufen.org
  User root
  IdentityFile ~/.ssh/blechhaufen-a.ed25519-sk
  IdentityFile ~/.ssh/blechhaufen-b.ed25519-sk

# home infrastructure
host homeassistant.dotdorm.net
  User hassio
host switch-1.dotdorm.net
  User admin
host *dotdorm.net
  User root
  IdentityFile ~/.ssh/home.ed25519

# VCS
host bitbucket.org
  HostName bitbucket.org
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host github.com
  HostName github.com
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host gitlab.com
  HostName gitlab.com
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host git.bufata-et.de
  HostName git.bufata-et.de
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host gitlab.kit.edu
  HostName gitlab.kit.edu
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host gitlab.net.scc.kit.edu
  HostName gitlab.net.scc.kit.edu
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host gitlab.itiv.kit.edu
  HostName gitlab.itiv.kit.edu
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host gitea.yellowant.de
  HostName gitea.yellowant.de
  Port 2022
  User webspace-user
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519
host codeberg.org
  HostName codeberg.org
  User git
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/vcs.ed25519

host *
  Port 22
  ServerAliveInterval 10
  ServerAliveCountMax 30

  # source: https://infosec.mozilla.org/guidelines/openssh#modern
  # Ensure KnownHosts are unreadable if leaked - it is otherwise easier to know which hosts your keys have access to.
  #HashKnownHosts yes
  # decided to not hash known_hosts to get better autocompletion
  HashKnownHosts no
  # Host keys the client accepts - order here is honored by OpenSSH
  HostKeyAlgorithms ssh-ed25519-cert-v01@openssh.com,ssh-ed25519,ssh-rsa-cert-v01@openssh.com,ssh-rsa,ecdsa-sha2-nistp521-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp384,ecdsa-sha2-nistp256

  KexAlgorithms curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256
  MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
  Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr

  IdentitiesOnly yes
  AddKeysToAgent 8h
