# Make NeoVIM default editor
if [ -n "$(command -v nvim)" ]; then
  export EDITOR='nvim'
fi

## Custom env-vars for certain operating systems
{%@@ if platform == "darwin" @@%}
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
{%@@ endif @@%}
