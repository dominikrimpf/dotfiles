
{%@@ if sshfs @@%}
## SSHFS Stuff
alias mount-fse-ref="mount-sshfs fse:/home/ref ~/mount/fse-ref fse-ref"
alias mount-fse-transfer="mount-sshfs fse:/home/transfer ~/mount/fse-transfer fse-transfer"
alias mount-fse-dominikr="mount-sshfs fse:/home/dominikr ~/mount/fse-dominikr fse-dominikr"
alias mount-fse-backup="mount-sshfs fse:/home/backup ~/mount/fse-backup fse-backup"
alias mount-asta-ndw="mount-sshfs asta:/data/ndw ~/mount/asta-ndw asta-ndw"
alias mount-asta-unifest="mount-sshfs asta:/data/unifest ~/mount/asta-unifest asta-unifest"
alias mount-asta-data="mount-sshfs asta:/data ~/mount/asta-data asta-data"
alias mount-fse="mount-fse-ref; mount-fse-transfer"
alias mount-asta="asta-ndw; asta-unifest"

function mount-sshfs {
if [ "$#" -ne 3 ];then
        echo "Illegal Number of Parameters"
        echo "Usage: $0 Remote-Path Local-Mount-Point Volume-Name"
else
        remotepath=$1
        mountpoint=$2
        mountlabel=$3
        if ! mount | grep -q "$remotepath " ; then
                if ! [ -d "$mountpoint" ]; then
                        mkdir -p "$mountpoint"
                fi
                sshfs "$remotepath" "$mountpoint" -o local,reconnect,defer_permissions,allow_other,noappledouble,auto_cache -o volname="$3"
                echo "$remotepath sucessfully mounted in $mountpoint"
        else
        mountpath=$(mount | grep "$remotepath" | cut -d" " -f 3)
        echo "$remotepath is already mounted in $mountpath."
        fi
fi
}

function umount-sshfs {
for mountpoint in $(mount | grep "osxfuse" | cut -d" " -f 3); do
        diskutil umount force $mountpoint
        echo "Unmounted $mountpoint"
done
}
{%@@ endif @@%}
