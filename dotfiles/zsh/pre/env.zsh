# History for 100000 commands
#set history size
export HISTSIZE=1000000000
#save history after logout
export SAVEHIST=$HISTSIZE

#append into history file
setopt INC_APPEND_HISTORY
#remove old entry from history if it is the same
setopt HIST_IGNORE_ALL_DUPS
#add timestamp for each entry
setopt EXTENDED_HISTORY
