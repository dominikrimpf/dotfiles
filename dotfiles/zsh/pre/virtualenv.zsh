## VIRTUALENV / VIRTUALENVWRAPPER
# set where virtual environments will live
export WORKON_HOME=$HOME/.virtualenvs
# set where projects will live
export PROJECT_HOME=$HOME/{{@@ dev_folder @@}}

# set right python for virtualenvwrapper
{%@@ if platform == "darwin" @@%}
export VIRTUALENVWRAPPER_PYTHON={{@@ brew_prefix @@}}/bin/python3
{%@@ else @@%}
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
{%@@ endif @@%}
