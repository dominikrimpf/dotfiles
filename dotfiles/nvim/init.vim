call plug#begin()
" Utilities
Plug 'jeffkreeftmeijer/neovim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'scrooloose/nerdtree'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'semanser/vim-outdated-plugins'
{%@@ if fzf @@%}
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
{%@@ endif @@%}
Plug 'mg979/vim-visual-multi'

" Coding-plugins
Plug 'Townk/vim-autoclose'
Plug 'sbdchd/neoformat'

" Syntax / Language Support plugins
Plug 'cespare/vim-toml'
Plug 'jvirtanen/vim-hcl'

" Application-plugins
Plug 'chr4/nginx.vim'

" Theming plugins
Plug 'kepano/flexoki-neovim'
Plug 'itchyny/lightline.vim'

" Initialize plugin system
call plug#end()

{%@@ if platform == "darwin" @@%}
" enable truecolor support
set termguicolors

{%@@ endif @@%}
" remap leader to space
let mapleader = "\<Space>"

" backspace fix
set backspace=indent,eol,start

" Show linenumbers
set number

" Disable Mode display
set noshowmode

" Set Proper Tabs
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" Enable highlighting of the current line
set cursorline

" Setup Live-Search with higlighting
set incsearch
set hlsearch

" NERDTree Config
let NERDTreeShowHidden=1
" Open NERDTree if vim is invoked without file
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" Close vim if only NERDTree is left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

{%@@ if fzf @@%}
" fzf mappings
nnoremap <leader><space> :Files<CR>
nnoremap <leader>gf :GFiles<CR>
nnoremap <leader>gs :GFiles?<CR>
nnoremap <leader>b :Buffers<CR>
nnoremap <leader>? :History<CR>
nnoremap <leader>/ :History/<CR>
{%@@ endif @@%}

" multi select mappings
let g:VM_maps = {}
let g:VM_maps['Find Under'] = '<C-m>' " replace C-n
let g:VM_maps['Find Subword Under'] = '<C-m>' " replace visual C-n

" THEMING
colorscheme flexoki-dark
" TODO: flexoki dark theme for lightline
let g:lightline = { 'colorscheme': 'deus' }

" Do not show any message if all plugins are up to date. 0 by default
let g:outdated_plugins_silent_mode = 1

" KEY-MAPPINGS
map <c-n> :NERDTreeToggle<CR>
set pastetoggle=<F2>

" MISC
autocmd Filetype gitcommit set textwidth=80

" gopass edit config
{%@@ if platform == "linux" @@%}
au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile
{%@@ elif platform == "darwin" @@%}
au BufNewFile,BufRead /private/**/gopass** setlocal noswapfile nobackup noundofile
{%@@ endif @@%}
